# details-rendering-problem

An illustration of the bug reported in gitlab-org/gitlab-ce#49229.

<details>

<summary>Click here to reveal/hide a Python script.</summary>

```python
print("Hello world")
```

</details>